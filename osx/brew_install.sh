#!/bin/bash

# Tap GitTown Keg
brew tap Originate/gittown

# Tap NeoVim
brew tap neovim/homebrew-neovim

shells=(
  bash
  fish
  zsh
  zsh-completions
  zsh-syntax-highlighting
)

security=(
  gnupg
  openssl
)

fetchers=(
  curl
  wget
)

search=(
  ack
  findutils
  the_silver_searcher
)

file_dir=(
  fasd
  rename
)

version_control=(
  git
  git-town
  hub
  subversion
  mercurial
  cvs
)

tmux=(
  tmux
  reattach-to-user-namespace
)

editors=(
  vim
  emacs
)


ruby=(
  rbenv
  ruby-build
)

node=(
  node
  nodejs
)

scala=(
  sbt
  scala
)

clojure=(
  leiningen
)

databases=(
  mongo
  postgresql
  sqlite
)

other=(
  automake
  caskroom/cask/brew-cask
  gcc
  heroku
  pkg-config
  pianobar
)

declare -a brew_packages=(
  ${shells[@]}
  ${security[@]}
  ${fetchers[@]}

  ${search[@]}
  ${file_dir[@]}

  ${version_control[@]}
  ${tmux[@]}
  ${editors[@]}

  ${cjojure[@]}
  ${ruby[@]}
  ${node[@]}
  ${scala[@]}

  ${databases[@]}
  ${other[@]}
)

length=${#brew_packages[@]}

for ((i=0; i<${length}; i++));
do
  brew install ${brew_packages[$i]}
done
