# Environment Setup

This aggregation of the tools I use in my different environments,
as well some scripts and files to get my environments setup.

The kind of tooling you'll get by installing this:

Vim
Emacs
Tiling Windows
Clipboard Plugin
Great Looking Linux Environments
