#!/bin/bash

workspace_managers=(
  amethyst
  spectacle
  totalspaces
)

other=(
  alfred
  bartender
  flux
  easysimbl
  virtualbox
  vagrant
  scroll-reverser
)

music=(
  spotify
  tomahawk
)

editors=(
  atom
  sublime-text
  mou
)

browsers=(
  google-chrome
  chromium
  firefox
)

terminal=(
  iterm2
)

chat=(
  adium
  slack
  hipchat
)

productivity=(
  evernote
  tomighty
)

declare -a brew_cask_packages=(
  ${workspace_managers[@]}
  ${browsers[@]}
  ${chat[@]}
  ${music[@]}
  ${productivity[@]}
  ${other[@]}
)

length=${#brew_cask_packages[@]}

for ((i=0; i<${length}; i++));
do
  brew cask install ${brew_cask_packages[$i]}
done
