#!/bin/bash

# Install Homebrew Package Manager
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Homebrew Packages
bash ./brew_install.sh

# Install Homebrew Cask Packages
bash ./brew_cask_install.sh
